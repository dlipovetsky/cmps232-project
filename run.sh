#!/usr/bin/env bash
round=$1
category=$2

case "$category" in
none)
  experiments=(h2c-multi-0ms-0%)
  ;;
delayonly)
  experiments=(h2c-multi-35ms-0%)
  ;;
lossonly)
  experiments=(h2c-multi-0ms-1%)
  ;;
delayandloss)
  experiments=(h2c-multi-35ms-1%)
  ;;
esac

for experiment in ${experiments[*]};
do
  ssh danl@node-1 "HTTPD=/usr/local/apache2/bin/httpd /users/danl/local-apache/bin/apachectl -k restart"
  python run.py $round $experiment
  git add output/$round
  git commit -m "Adds $experiment to round $round"
  git push origin master
done
