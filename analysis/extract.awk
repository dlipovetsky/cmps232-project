#!/usr/bin/awk -f
# usage:
# cp template.csv test.csv; find ../output/ -name "*.out" | xargs -n 1 ./extract.awk >> test.csv

# dirname and basename from https://github.com/cheusov/runawk/
function dirname (pathname){
	if (!sub(/\/[^\/]*\/?$/, "", pathname))
		return "."
	else if (pathname != "")
		return pathname
	else
		return "/"
}
function basename (pathname, suffix){
	sub(/\/$/, "", pathname)
	if (pathname == "")
		return "/"

	sub(/^.*\//, "", pathname)

	if (suffix != "" && has_suffix(pathname, suffix))
		pathname = substr(pathname, 1, length(pathname) - length(suffix))

	return pathname
}

BEGIN {
    FS=", "
    OFS=","
    ORS=","
}

/h2load/ {
    gsub(/h2load http\:\/\/node\-1\:8080\//,"",$1)
    gsub(/-p /,",",$1)
    gsub(/-t /,",",$1)
    gsub(/-c /,",",$1)
    gsub(/-m /,",",$1)
    gsub(/-n /,",",$1)
    gsub(/-T.*$/,"",$1)
    gsub(/ /,"",$1)

    split($1, fields, ",")
    if (fields[1] ~ /.*kb/)
        normalized_obj = fields[1] * 1
    else if (fields[1] ~ /.*mb/)
        normalized_obj = fields[1] * 1000

    split(basename(dirname(FILENAME)), addl_info, "-")
    experiment = addl_info[1] addl_info[2]
    delay = addl_info[3]
    sub(/ms/,"",delay)

    packet_loss = addl_info[4]

    print experiment, delay, packet_loss, normalized_obj, fields[2], fields[3], fields[4], fields[5], fields[6]
}

/finished in / {
    finished=1

    gsub(/finished in /,"",$1);
    gsub(/ req\/s/,"",$2);
    gsub(/ /,"",$1)
    gsub(/ /,"",$2)


    # normalize time to ms
    if ($1 ~ /.*ms/)
        normalized_time=$1 * 1;
    else
        normalized_time=$1 * 1000;

    # normalize tput to MB/s
    if ($3 ~ /.*GB\/s/)
        normalized_tput = $3 * 1000;
    else if ($3 ~ /.*MB\/s/)
        normalized_tput = $3 * 1;
    else if ($3 ~ /.*KB\/s/)
        normalized_tput = $3 / 1000;
    else
        exit 1 # unknown rate

    print normalized_time,$2,normalized_tput
}

/requests:/ {
    gsub(/requests: /,"",$1);
    gsub(/ total/,"", $1);
    gsub(/ started/,"", $2);
    gsub(/ done/,"", $3);
    gsub(/ succeeded/,"", $4);
    gsub(/ failed/,"", $5);
    gsub(/ errored/,"", $6);
    gsub(/ timeout/,"", $7);

    print $1,$2,$3,$4,$5,$6,$7;
}

/status codes: / {
    gsub(/status codes: /,"",$1);
    gsub(/ 2xx/,"",$1);
    gsub(/ 3xx/,"",$2);
    gsub(/ 4xx/,"",$3);
    gsub(/ 5xx/,"",$4);
    print $1,$2,$3,$4;
}

/traffic: / {
    gsub(/traffic: /,"",$1);
    gsub(/ bytes total/,"",$1);
    gsub(/ bytes headers/,"",$2);
    gsub(/ \(space savings/,"",$2);
    gsub(/\)/,"",$2);
    gsub(/^ /,"",$2);
    gsub(/ /,",",$2);
    gsub(/ bytes data/,"",$3);

    ORS="\n"
    print $1,$2,$3
}

END {
    # if we never finished (run was cancelled during "progress" output), then output 'infinite' time to complete
    if (finished==0) {
        ORS="\n"
        print "99999"
    }
}
