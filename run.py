from datetime import datetime
from itertools import product
import os
import subprocess
import sys

def run_tc(latency, packetLoss):
  tc_cmd = "sudo tc qdisc change dev eth1 root netem delay {}ms loss {}".format(latency, packetLoss)
  retval = subprocess.call(tc_cmd.split())
  return retval

def run_h2load(proto, obj, numThreads, numClients, numStreams, numRequests, name):
  # longest runs so far have been under 20m, so let's set a 30m timeout
  connection_active_timeout = "30m"

  uri = 'http://node-1:8080/' + obj
  h2load_cmd = "h2load {} -p {} -t {} -c {} -m {} -n {} -T {}".format(uri, proto, numThreads, numClients, numStreams, numRequests, connection_active_timeout)
  
  outFile = open(name + '.out', 'w')
  outFile.write(h2load_cmd)
  outFile.write('\n')
  outFile.close()
  outFile = open(name + '.out', 'a')
  errFile = open(name + '.err', 'w')
  
  retval = subprocess.call(h2load_cmd.split(), stdout=outFile, stderr=errFile)
  
  outFile.close()
  errFile.close()
  return retval

def name_experiment(*args):
  return "-".join(str(a).replace('/','-') for a in args)

def do_experiments(xs):
  for x in xs:
    name = name_experiment(*x)
    run_tc(x[6], x[7])
    args = x[0:6] + (name,)
    run_h2load(*args)

def multiclient(xs):
  """Ensures numStreams * numClients = numRequests"""
  res = []
  for e in xs:
    numStreams = e[5] / e[3]
    e2 = e[0:4] + (numStreams,) + e[5:]
    res.append(e2)
  return set(res)

def ensureDir(path):
  if not os.path.exists(path):
    os.makedirs(path)

def enumerate_experiments():
  objs = ['1kb', '16kb', '128kb', '1mb', '8mb', '64mb', '128mb']
  numsThreads = [1]
  numsClients = [2**i for i in xrange(0,8)]
  numsStreams = [2**i for i in xrange(0,8)]
  numsRequests = [2**i for i in xrange(7,8)]
  latencies = [0, 35]
  packetLosses = [0, 1]

  experiments = dict()

  # no delay or packet loss
  experiments['http/1.1-0ms-0%'] = product(['http/1.1'], objs, numsThreads, numsClients, [1], numsRequests, [0], [0])
  experiments['h2c-singleclient-0ms-0%'] = product(['h2c'], objs, numsThreads, [1], numsStreams, numsRequests, [0], [0])
  experiments['h2c-multi-0ms-0%'] = multiclient(product(['h2c'], objs, numsThreads, [2,4,8,16], numsStreams, numsRequests, [0], [0])))

  # delay, no packet loss
  experiments['http/1.1-35ms-0%'] = product(['http/1.1'], objs, numsThreads, numsClients, [1], numsRequests, [35], [0])
  experiments['h2c-singleclient-35ms-0%'] = product(['h2c'], objs, numsThreads, [1], numsStreams, numsRequests, [35], [0])
  experiments['h2c-multi-35ms-0%'] = multiclient(product(['h2c'], objs, numsThreads, [2,4,8,16], numsStreams, numsRequests, [35], [0]))

  # packet loss, no delay
  experiments['http/1.1-0ms-1%'] = product(['http/1.1'], objs, numsThreads, numsClients, [1], numsRequests, [0], [1])
  experiments['h2c-singleclient-0ms-1%'] = product(['h2c'], objs, numsThreads, [1], numsStreams, numsRequests, [0], [1])
  experiments['h2c-multi-0ms-1%'] = multiclient(product(['h2c'], objs, numsThreads, [2,4,8,16], numsStreams, numsRequests, [0], [1]))

  # both delay and packet loss
  experiments['http/1.1-35ms-1%'] = product(['http/1.1'], objs, numsThreads, numsClients, [1], numsRequests, [35], [1])
  experiments['h2c-singleclient-35ms-1%'] = product(['h2c'], objs, numsThreads, [1], numsStreams, numsRequests, [35], [1])
  experiments['h2c-multi-35ms-1%'] = multiclient(product(['h2c'], objs, numsThreads, [2,4,8,16], numsStreams, numsRequests, [35], [1]))

  experiments['test'] = [('http/1.1', '1kb', '1', '1', '1', '512', '0', '0.0')]

  return experiments


if __name__ == '__main__':
  if len(sys.argv) >= 3:
    xsRound = sys.argv[1]
    experiments = enumerate_experiments()
    outputRootDir = 'output'
    for xsName in sys.argv[2:]:
      xs = experiments.get(xsName)
      if xs:
        xsOutputDir = os.path.join(os.getcwd(), outputRootDir, xsRound, xsName.replace('/','-'))
        ensureDir(xsOutputDir)
        os.chdir(xsOutputDir)
        timestamp = datetime.strftime(datetime.now(),'%Y-%m-%d %H:%M:%S')
        print("{} -- round {}, experiment {}, output to {}".format(timestamp, xsRound, xsName, xsOutputDir))
        do_experiments(xs)
      else:
        print("unknown experiment {}".format(xsName))
  else:
    print("usage: run.py <round> <experiment name>")
